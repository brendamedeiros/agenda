import java.util.*;

public class Agenda {

    private ArrayList<Contato> listaContatos;

	public static void main (String[] Args){
	
	//Declaração
	Scanner lerTeclado = new Scanner(System.in); 
	Contato umContato = new Contato();
	ArrayList<Contato> contatos = new ArrayList<Contato>();

	System.out.println("Digite o nome do Contato:");
	String umNome = lerTeclado.nextLine();
	umContato.setNome(umNome);

	System.out.println("Digite o telefone do contato:");
	String umTelefone = lerTeclado.nextLine();
	umContato.setTelefone(umTelefone);

	System.out.println("Informe o sexo do Contato: (use 0 para feminino e 1 para masculino)");
	int umSexo = Integer.parseInt (lerTeclado.nextLine());	
	umContato.setSexo(umSexo);

	contatos.add(umContato);

	System.out.println("Contato inserido com sucesso!");
	System.out.println("Nome: " + umContato.getNome());
	System.out.println("Telefone: " + umContato.getTelefone());
	System.out.println("Sexo: " +umContato.getSexo());

	}
}