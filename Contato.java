public class Contato{

	private String nome;
	private String telefone;
	private int sexo; //0 - masculino | 1 - feminino
	private String email;

	//métodos acessadores dos atributos(setters e getters)
	
	public void setNome (String nome){
	    this.nome=nome;
	}

	public String getNome (){
	    return nome;
	}

	public void setTelefone (String telefone){
	    this.telefone=telefone;
	}

	public String getTelefone (){
	    return telefone;
	}

	public void setSexo (int sexo){
	    this.sexo = sexo;
	}
	
	public int getSexo (){
	    return sexo;
	}
	
	public void setEmail (String email){
	    this.email = email;
	}

}